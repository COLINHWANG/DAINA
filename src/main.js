import Vue from 'vue'
import App from './App.vue'
import VueRouter from "vue-router"
import VueResource from "vue-resource"
import Vuex from 'vuex'
import ElementUI from 'element-ui';
import MintUI from 'mint-ui';
import axios from 'axios';
Vue.prototype.$http = axios;
import 'element-ui/lib/theme-chalk/index.css';
import 'mint-ui/lib/style.css';
import VideoPlayer from 'vue-video-player'


import { Field } from 'mint-ui';
import { MessageBox } from 'mint-ui';

import home from './components/home.vue'
import register from './components/register.vue'
import Counter from './components/Counter.vue'
import news from './components/news.vue'
import guanli from './components/guanli.vue'
import message from './components/message.vue'
import login from './components/login.vue'
import zhongxin from './components/zhongxin.vue'
import pingjia from './components/pingjia.vue'
import fabu02 from './components/fabu02.vue'
import MoreSetting from './components/MoreSetting.vue'
import jiedan from './components/jiedan.vue'
import guanggao from './components/guanggao.vue'
import xiangqing from './components/xiangqing.vue'
import renzhen from './components/renzhen.vue'
import store from './store.js'
Vue.config.debug = true;
Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(Vuex);
Vue.use(ElementUI);
Vue.use(MintUI);
Vue.use(VideoPlayer)
require('video.js/dist/video-js.css')
require('vue-video-player/src/custom-theme.css')

const router = new VueRouter({
    //mode: 'history',
    base: __dirname,
    routes: [
      {
        path: '/',
        component: home,
      },
      {
        path: '/home',
        component: home,
      },
      {
        path: '/register',
        component: register
      },
      {
        path: '/message',
        component: message
      },
      {
        path: '/Counter',
        component: Counter
      },
      {
        path: '/login',
        component: login
      },
      {
        path: '/zhongxin',
        component: zhongxin
      },
      {
        path: '/fabu02',
        component: fabu02
      },
      {
        path: '/pingjia',
        component: pingjia
      },
      {
        path: '/news',
        component: news
      },
      {
        path: '/MoreSetting',
        component: MoreSetting
      },
      {
        path: '/jiedan',
        component: jiedan
      },
      {
        path: '/guanggao',
        component: guanggao
      },
      {
        path: '/guanli',
        component: guanli
      },
      {
        path:'/xiangqing',
        component: xiangqing
      },
      {
      path: '/renzhen',
      component: renzhen
      },
    ]
  })

  const app = new Vue({
      router: router,
      store,
      render: h => h(App)
  }).$mount('#app')